import os
import time
import requests
import logging

# Load environment variables
GENAI_API_KEY = os.environ.get('GENAI_API_KEY')
if not GENAI_API_KEY:
    raise KeyError('GENAI_API_KEY not found in environment variables')

GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
if not GITLAB_TOKEN:
    raise KeyError('GITLAB_TOKEN not found in environment variables')

PROJECT_ID = os.environ.get('CI_PROJECT_ID')
MR_ID = os.environ.get('CI_MERGE_REQUEST_IID')

GENAI_API_URL = "https://api.genai.com/v1/summarize"  # Replace with the actual GenAI API URL

def get_diff():
    with open('diff.txt', 'r') as file:
        return file.read()

def process_diff_with_genai(diff, retries=3, delay=60):
    headers = {
        'Authorization': f'Bearer {GENAI_API_KEY}',
        'Content-Type': 'application/json',
    }
    data = {
        'text': diff,
        'summary_length': 'short'  # Adjust parameters as needed for GenAI API
    }
    for attempt in range(retries):
        try:
            response = requests.post(GENAI_API_URL, headers=headers, json=data)
            response.raise_for_status()
            return response.json().get('summary', '')
        except requests.exceptions.HTTPError as e:
            if response.status_code == 429:  # Rate limit error
                print(f"RateLimitError: {e}. Retrying in {delay} seconds... (Attempt {attempt + 1}/{retries})")
                time.sleep(delay)
            else:
                raise e
    raise Exception("Exceeded maximum retries for GenAI API due to rate limit.")

def update_merge_request_description(description):
    url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/merge_requests/{MR_ID}'
    headers = {'PRIVATE-TOKEN': GITLAB_TOKEN}
    data = {'description': description}
    response = requests.put(url, headers=headers, json=data)
    if response.status_code != 200:
        print(f"Failed to update merge request: {response.status_code}, {response.text}")

if __name__ == '__main__':
    diff = get_diff()
    if not diff:
        print("No changes detected in diff.txt.")
    else:
        description = process_diff_with_genai(diff)
        update_merge_request_description(description)
